<?php
    require_once ("Ape.php");
    require_once ("frog.php");
    $object = new Animal ("domba");

    echo "Nama Hewan :". $object -> name." <br>";
    echo "Jumlah kaki :". $object -> legs ."<br>";
    echo  "Berdarah dingin :". $object -> cold_blooded."<br> <br>";

    $sungokong = new Ape("kera sakti");

    echo "Nama Hewan :". $sungokong -> name." <br>";
    echo "Jumlah kaki :". $sungokong -> legs ."<br>";
    echo  "Berdarah dingin :". $sungokong -> cold_blooded."<br>";
    echo $sungokong -> yell(). "<br> <br>";

    $kodok = new frog ("Buduk");

    echo "Nama Hewan :". $kodok -> name." <br>";
    echo "Jumlah kaki :". $kodok -> legs ."<br>";
    echo  "Berdarah dingin :". $kodok -> cold_blooded."<br>";
    echo $kodok -> jump();
?>